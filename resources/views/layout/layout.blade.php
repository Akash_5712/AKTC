<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  
   @include('layout.style')
</head>

<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">

  @include('layout.header')

  @include('layout.sidebar')

  <div class="content-wrapper">
      @yield('content')
  </div>
  
  @include('layout.left-sidebar')

  <div class="control-sidebar-bg"></div>

</div>

@include('layout.footer')
</body>
</html>
