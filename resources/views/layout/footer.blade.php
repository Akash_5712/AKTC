<!-- jQuery 3 -->
<?=Html::script('js/jquery.min.js')?>
<!-- jQuery UI 1.11.4 -->
<?=Html::script('js/jquery-ui.min.js')?>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<?=Html::script('js/bootstrap.min.js')?>
<!-- Morris.js charts -->
<?=Html::script('js/raphael.min.js')?>
<?=Html::script('js/morris.min.js')?>
<!-- Sparkline -->
<?=Html::script('js/jquery.sparkline.min.js')?>
<!-- jQuery Knob Chart -->
<?=Html::script('js/jquery.knob.min.js')?>
<!-- daterangepicker -->
<?=Html::script('js/moment.min.js')?>
<?=Html::script('js/daterangepicker.js')?>
<!-- datepicker -->
<?=Html::script('js/bootstrap-datepicker.min.js')?>
<!-- Bootstrap WYSIHTML5 -->
<?=Html::script('js/bootstrap3-wysihtml5.all.min.js')?>
<!-- Slimscroll -->
<?=Html::script('js/jquery.slimscroll.min.js')?>
<!-- FastClick -->
<?=Html::script('js/fastclick.js')?>
<!-- AdminLTE App -->
<?=Html::script('js/adminlte.min.js')?>
<!-- AdminLTE for demo purposes -->
<?=Html::script('js/demo.js')?>
@yield('script')