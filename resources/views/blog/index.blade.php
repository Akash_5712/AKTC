@extends('layout.layout')
@section('style')
<?=Html::style('css/dataTables.bootstrap.min.css')?>
<?= Html::style('css/toastr.min.css') ?>
@stop
@section('content')
<div class="box">
<div class="box-header">
  <h3 class="box-title">CMS Pages</h3>
</div>
<!-- /.box-header -->
<div class="box-body">
  <table id="blog" class="table table-bordered table-striped">
    <thead>
	    <tr>
	      <th>Title</th>
	      <th></th>
	      <th>Description</th>
	    </tr>
    </thead>
  </table>
</div>
<!-- /.box-body -->
</div>
@stop


@section('script')
    <?=Html::script('js/jquery.dataTables.min.js')?>
	<?=Html::script('js/dataTables.bootstrap.min.js')?>
	<?=Html::script('js/fnStandingRedraw.js')?>
    <?=Html::script('js/toastr.min.js')?>
<script>
    var table = "blog";
    var title = "Are you sure to delete this User?";
    var text = "You will not be able to recover this record";
    var type = "warning";
    var token = "<?=csrf_token()?>";
    
        blog= $('#'+table).DataTable({
        "bProcessing": false,
        "bServerSide": true,
        "autoWidth": false,
        "sAjaxSource": "<?=route('admin.blog')?>",
        "aaSorting": [ 1,"desc"],
        "aoColumns": [  

          {   
                mData:"title",bSortable : true,sClass : 'text-center',sWidth:"10%",
                mRender : function(v,t,o){
                    var edit_path = "<?=URL::route('admin.blog.edit',['id' => ':id'])?>";
                    edit_path = edit_path.replace(':id',o['id']);
                    var act_html  = '<a title="Edit '+o['title']+'" href="'+edit_path+'">'+ v +'</a>';
                    return act_html;
                },
            },

            {
                        mData: "updated_at",
                        bVisible:false,
            }, 

            {   mData:"description",bSortable : true,sClass : 'text-center',sWidth:"10%" },
                                     
        ],
        fnPreDrawCallback : function() { $("div.overlay").css('display','flex'); },
        fnDrawCallback : function (oSettings) {
            $("div.overlay").hide();
        },
    });
</script>
@include('layout.alert')
@stop








