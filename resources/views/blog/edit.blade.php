@extends('layout.layout')

@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box blue-border">
                <div class="box-header with-border">
                    <h4 class="box-title">CMS Details</h4>
                </div>
<?=Form::model($blog, ['method' => 'PUT','route' => ['admin.blog.update', $blog->id], 'class' => 'm-0 form-horizontal'])?>
	<div class="box-body">
		@include('blog.form')
	</div>

<div class="box-footer">
	<div class="from-group">
		<lable class="col-md-2"></lable>
		<div class="col-md-8 text-right">
    		<button type="submit" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn save_btn" title="Save and exit">Save & exit</button>
    		<a href="<?=URL::route('admin.blog')?>" class="btn btn-default btn-sm" title="Back to user Page">Cancel</a>
		</div>
	</div>
</div>

    </div>
  </div>
</div>
</section>
<?=Form::close()?>
@stop

@section('script')
<script src="https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>

        <script type="text/javascript">
        CKEDITOR.replace( 'description', {
            height: 300,

            // Configure your file manager integration. This example uses CKFinder 3 for PHP.
            filebrowserImageBrowseUrl: '/admin/filemanager?type=Images',
            filebrowserImageUploadUrl: '/admin/filemanager/upload?type=Images&_token={{csrf_token()}}',
            filebrowserBrowseUrl: '/admin/filemanager?type=Files',
            filebrowserUploadUrl: '/admin/filemanager/upload?type=Files&_token={{csrf_token()}}',


            toolbar: [
                { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike'] },
                { name: 'paragraph', items : [ 'NumberedList','BulletedList'] },
                { name: 'insert', items : [ 'Image','Table' ] },
            ],
            removeButtons: '',
            image_previewText: ' '
        });
 </script>       
@stop
