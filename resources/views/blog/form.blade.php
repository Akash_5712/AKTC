
<input type="hidden" name="id" value="">
    <div class="form-group @if($errors->has('title')) {{ 'has-error' }} @endif">
        <div class="control-label col-md-2">Title<sup class="text-danger">*</sup></div>
        <div class="col-md-8">
        <?=Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title']);?>
        <span id="title_error" class="help-inline text-danger"><?=$errors->first('title')?></span>
    </div>
    </div>
    <div class="form-group @if($errors->has('description')) {{ 'has-error' }} @endif">
        <div class="control-label col-md-2">Description<sup class="text-danger">*</sup></div>
        <div class="col-md-8">
        <?=Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description','id' => 'description']);?>
        <span id="description_error" class="help-inline text-danger"><?=$errors->first('description')?></span>
    </div>
    </div>
                    


