<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Blog;

class AdminController extends Controller
{
 	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index(Request $request)
    {
      if ($request->ajax()) {

        $where_str = "1 = ?";
        $where_params = array(1);

        if (!empty($request->input('sSearch'))) {
            $search = $request->input('sSearch');
            $where_str .= " and ( title like \"%{$search}%\""
                . " or description like \"%{$search}%\""
                . ")";
        }
        $columns = array('id','updated_at','title','description');

        $employee_count = Blog::select('*')
            ->whereRaw($where_str, $where_params)
            ->count();

        $employee = Blog::select($columns)
            ->whereRaw($where_str, $where_params);

        if ($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != '') {
            $employee = $employee->take($request->input('iDisplayLength'))
                ->skip($request->input('iDisplayStart'));
        }

        if ($request->input('iSortCol_0')) {
            $sql_order = '';
            for ($i = 0; $i < $request->input('iSortingCols'); $i++) {
                $column = $columns[$request->input('iSortCol_' . $i)];
                if (false !== ($index = strpos($column, ' as '))) {
                    $column = substr($column, 0, $index);
                }
                $employee = $employee->orderBy($column, $request->input('sSortDir_' . $i));
            }
        }
        $employee = $employee->get();
        $response['iTotalDisplayRecords'] = $employee_count;
        $response['iTotalRecords'] = $employee_count;
        $response['sEcho'] = intval($request->input('sEcho'));
        $response['aaData'] = $employee->toArray();

        return $response;
    }
    return view('blog.index');   
	}

 	public function edit($id)
    {	

        $blog=Blog::find($id);
        return view('blog.edit',compact('blog'));
    }

     public function update(Request $request,$id)
    {	
    	$blog_data=$request->all();
 
       	$blog_data_save=Blog::firstOrNew(['id'=>$id]);
        $blog_data_save->fill($blog_data);
        $blog_data_save->save();

        return redirect()->route('admin.blog')->with('message','Update Blog Successfully')->with('message_type','success');
    }

}



