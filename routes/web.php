<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Route::get('reset', function () {
//     return view('admin-reset');
// });
Route::post('admin-password/email', 'Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
Route::get('admin/reset/{token?}', 'Admin\ResetPasswordController@showResetForm')->name('password.reset.admin');
Route::post('admin/password/reset','Admin\ResetPasswordController@reset')->name('admin.password.reset');
Route::prefix('admin')->group(function(){


Route::get('/login','Admin\AdminloginController@showLoginForm')->name('admin.login');
Route::post('/login','Admin\AdminloginController@login');

Route::group(array('middleware' => ['admin_guest']), function () {

	Route::get('/logout', 'Admin\AdminloginController@logout')->name('admin.logout');
	Route::get('dashboard','DashboardController@index')->name('admin.dashboard');
	Route::get('blog','Admin\AdminController@index')->name('admin.blog');
	Route::get('blog/{id}/edit','Admin\AdminController@edit')->name('admin.blog.edit');
	Route::put('blog/{id}','Admin\AdminController@update')->name('admin.blog.update');

	});
});





